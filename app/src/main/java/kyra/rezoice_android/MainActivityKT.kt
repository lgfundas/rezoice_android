package kyra.rezoice_android

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivityKT : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.country_selection)
    }

    override fun onStart() {
        super.onStart()
        Log.d("android", "In Start")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("android", "In ReStart")
    }

    override fun onStop() {
        super.onStop()
        Log.d("android", "In Stop")
    }

    override fun onResume() {
        super.onResume()
        Log.d("android", "In Resume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("android", "In Pause")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("android", "In Destroy")
    }
}
