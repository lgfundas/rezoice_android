package kyra.rezoice_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etEmail, etPassword;
    Button btnLogin;
    TextView tvSignup, tvChangeCountry;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);

        initializeViews();

        Button accept = (Button) findViewById(R.id.login);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(myIntent);
            }
        });

    }

    private void initializeViews() {
        etEmail = findViewById(R.id.email);
        etPassword = findViewById(R.id.password);
        tvChangeCountry = findViewById(R.id.changeCountry);
        tvSignup = findViewById(R.id.signup);
        btnLogin = findViewById(R.id.login);

        btnLogin.setOnClickListener(this);
        tvSignup.setOnClickListener(this);
        tvChangeCountry.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login: {
                break;
            }
            case R.id.signup: {
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
                break;
            }
            case R.id.changeCountry: {
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
                finish();
                break;
            }
        }

    }
}
