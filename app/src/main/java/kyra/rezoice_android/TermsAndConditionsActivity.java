package kyra.rezoice_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class TermsAndConditionsActivity extends AppCompatActivity {

    CheckBox cbTnc, cbpp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_conditions);

        cbTnc = findViewById(R.id.cbtnc);
        cbpp = findViewById(R.id.cbpp);

        Button accept = (Button) findViewById(R.id.acceptTnc);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cbpp.isChecked() && cbTnc.isChecked()) {
                    Intent myIntent = new Intent(TermsAndConditionsActivity.this, LoginActivity.class);
                    startActivity(myIntent);
                    finish();
                } else {
                    Toast.makeText(TermsAndConditionsActivity.this, "Please read tnc and check those boxes", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
